package RunnerUtility

import (
	"time"

	"gopkg.in/robfig/cron.v2"
)

type StatusData struct {
	Id            cron.EntryID
	RunnerUtility *Job
	Next          time.Time
	Prev          time.Time
}

// Return detailed list of currently running recurring jobs
// to remove an entry, first retrieve the ID of entry
func Entries() []cron.Entry {
	return MainCron.Entries()
}

func StatusPage() ([]StatusData, error) {
	entries := MainCron.Entries()
	Statuses := make([]StatusData, len(entries))
	for k, v := range entries {
		Statuses[k].Id = v.ID
		Statuses[k].RunnerUtility = AddJob(v.Job)
		Statuses[k].Next = v.Next
		Statuses[k].Prev = v.Prev
	}
	return Statuses, nil
}

func StatusJson() map[string]interface{} {
	value, _ := StatusPage()
	return map[string]interface{}{
		"appstatuscontroller": value,
	}

}

func AddJob(job cron.Job) *Job {
	return job.(*Job)
}
